from django.views.generic.base import RedirectView

from qa.models import Question


class IndexView(RedirectView):
    def get_redirect_url(self):
        return Question.objects.filter(parent__isnull=True).first().get_absolute_url()