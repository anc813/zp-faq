from django.conf.urls import include, url
from django.contrib import admin
from django.views.debug import default_urlconf

from posts.views import UserPostsView
from . import views
from django.contrib.sitemaps.views import sitemap
from qa.sitemap import QuestionSitemap


urlpatterns = [
    # Examples:
    url(r'^$', views.IndexView.as_view()),
    # url(r'^$', 'profit.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^qa/', include("qa.urls", namespace="qa")),
    url(r'^posts/$', UserPostsView.as_view()),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),

    url(r'^sitemap\.xml$', sitemap, {'sitemaps': {"faq": QuestionSitemap}}, name='django.contrib.sitemaps.views.sitemap')

]

# rest api
#
#urlpatterns += [    
#]
