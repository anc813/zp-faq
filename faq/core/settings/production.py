from .base import *

DEBUG = False

ALLOWED_HOSTS = ['zp-faq.pp.ua', 'new.zp-faq.pp.ua', 'www.zp-faq.pp.ua']

INSTALLED_APPS += [
    'raven.contrib.django.raven_compat',
]

import os
import raven

RAVEN_CONFIG = {
    'dsn': 'https://56a34d42f9894c4992eac1263c3d2709:23863345768646d1a83cc000a632c0d8@sentry.io/236552',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    # 'release': raven.fetch_git_sha(os.path.dirname(os.path.dirname(os.pardir))),
}