# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-28 19:10
from __future__ import unicode_literals

import ckeditor.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('qa', '0016_auto_20171028_1910'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalQuestion',
            fields=[
                ('id', models.IntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('name', models.CharField(max_length=2048, verbose_name='Название')),
                ('button_name', models.CharField(blank=True, help_text='Необзяательно, если что будет такое же как и название', max_length=128, verbose_name='Текст на кнопке')),
                ('slug', models.SlugField(max_length=128)),
                ('created_at', models.DateTimeField(blank=True, editable=False)),
                ('updated_at', models.DateTimeField(blank=True, db_index=True, editable=False)),
                ('content', ckeditor.fields.RichTextField(blank=True, verbose_name='Текст')),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('id_development', models.BooleanField(default=True, help_text='Это чтобы мы сами понимали что ещё осталось, и давали понять остальным. Когда кажется что всё готово - просто снять эту галочку', verbose_name='В разработке')),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='qa.Question')),
            ],
            options={
                'verbose_name': 'historical Вопрос',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
    ]
