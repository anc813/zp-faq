# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-27 15:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0008_auto_20171027_1519'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='slug',
            field=models.SlugField(max_length=128),
        ),
    ]
