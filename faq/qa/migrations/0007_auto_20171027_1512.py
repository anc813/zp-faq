# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-27 15:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0006_auto_20171027_1059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='button_name',
            field=models.CharField(blank=True, help_text='Необзяательно, если что будет такое же как и название', max_length=128, verbose_name='Текст на кнопке'),
        ),
    ]
