from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def indent(times):
    return mark_safe('<span class="pl-3"></span>' * times)
