from django.views.decorators.cache import cache_page
from django.utils.decorators import available_attrs
from functools import wraps

def cache_for_anonim(timeout):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):

            if request.user.is_authenticated():
                return (view_func)(request, *args, **kwargs)
            else:
                return cache_page(timeout)(view_func)(request, *args, **kwargs)
        return _wrapped_view
    return decorator
