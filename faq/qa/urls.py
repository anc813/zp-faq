from django.conf.urls import include, url
from django.contrib import admin
from django.views.debug import default_urlconf

from .utlis import cache_for_anonim
from . import views
from django.conf import settings

urlpatterns = [
    # Examples:
    url(r'^(?P<pk>\d+)-(?P<slug>[\w-]+)/$', cache_for_anonim(60*5)(views.QuestionView.as_view()), name="question"),
    url(r'^sitemap/$', cache_for_anonim(60*5)(views.QuestionmapView.as_view()), name="questionmap"),
]
