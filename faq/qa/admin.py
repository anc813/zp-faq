from django.contrib import admin

# Register your models here.
from django_mptt_admin.admin import DjangoMpttAdmin
from . import models
from simple_history.admin import SimpleHistoryAdmin


class QuestionAdmin(SimpleHistoryAdmin, DjangoMpttAdmin):
    prepopulated_fields = {"slug": ("name",)}
    list_per_page = 500
    exclude = 'deleted_at', 'created_at', 'updated_at',
    filter_horizontal = 'related',
    list_filter = 'id_development',
    list_display = "button_name", "updated_at", 'id_development',

    def get_queryset(self, request):
        qs = super(QuestionAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(deleted_at__isnull=True)
        return qs

admin.site.register(models.Question, QuestionAdmin)
