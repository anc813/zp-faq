from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from . import models


class QuestionView(DetailView):
    model = models.Question
    queryset = models.Question.objects.filter(deleted_at__isnull=True)


class QuestionmapView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        data = super(QuestionmapView, self).get_context_data(**kwargs)
        data['root'] = models.Question.objects.all()
        return data
