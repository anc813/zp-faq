from django.contrib.sitemaps import Sitemap
from .models import Question


class QuestionSitemap(Sitemap):
    changefreq = "hourly"
    priority = 0.5

    def items(self):
        return Question.objects.filter(deleted_at__isnull=True)

    def lastmod(self, obj):
        return obj.updated_at

