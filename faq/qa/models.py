from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
from simple_history import register


# Create your models here.


class Question(MPTTModel):
    name = models.CharField("Название", max_length=2048)
    button_name = models.CharField("Текст на кнопке", max_length=128, blank=True, help_text="Необзяательно, если что будет такое же как и название")
    slug = models.SlugField(max_length=128)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True, verbose_name="Родительский элемент")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)
    content = RichTextField(blank=True, verbose_name="Текст")
    deleted_at = models.DateTimeField(blank=True, null=True)

    id_development = models.BooleanField("В разработке", default=True, help_text="Это чтобы мы сами понимали что ещё осталось, и давали понять остальным. Когда кажется что всё готово - просто снять эту галочку")

    related = models.ManyToManyField("self", symmetrical=False, verbose_name="Связанные ссылки", blank=True, help_text="Другие ссылки, которые могут быть полезны в этом контексте.")

    def save(self, *args, **kwargs):
        if not self.button_name:
            self.button_name = self.name
        super(Question, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("qa:question", kwargs={"pk": self.pk, "slug": self.slug})

    def __str__(self):
        return "%s%s" % (self.button_name, (" [в разработке]" if self.id_development else ""))

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = "Вопрос"
        verbose_name_plural = "Вопросы"


register(Question)