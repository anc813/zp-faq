from django.db import models


# Create your models here.
class Post(models.Model):
    post_id = models.IntegerField(unique=True)
    date = models.DateField()
    username = models.CharField(max_length=255, db_index=True)
    full_body = models.TextField()
    body = models.TextField()

    class Meta:
        ordering = "-post_id",

