from django.core.management.base import BaseCommand

import os
from posts.models import Post
from posts.services import fetch_new_posts


class Command(BaseCommand):
    help = 'get new posts'

    def handle(self, *args, **options):
        last_post_id = getattr(Post.objects.order_by("-post_id").first(), "post_id", None)
        fetch_new_posts(last_post_id)