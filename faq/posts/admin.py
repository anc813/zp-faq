from django.contrib import admin
from . import models


# Register your models here.
@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    list_display = 'post_id', 'date', 'username'
    search_fields = 'username', "post_id"