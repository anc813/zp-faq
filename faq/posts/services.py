import grab
import dateparser
from weblib.error import DataNotFound
from .models import Post


def fetch_new_posts(last_post_id=None):
    url = "https://www.turpravda.ua/forums/discussion/comment/{post_id}/#Comment_{post_id}".format(post_id=last_post_id)
    if not last_post_id:
        url = "https://www.turpravda.ua/forums/discussion/189482/zaderzhka_vydachi_zagranpasporta/p1"
    g = grab.Grab()

    while True:
        response = g.go(url)

        if response.url.endswith("/404.php") and last_post_id:
            return Post.objects.filter(post_id=last_post_id).delete()

        assert response.code == 200

        selector = "//*//ul[contains(@class, 'Comments')]/li[contains(@class, 'ItemComment')]"
        for comment in response.select(selector):
            id = int(comment.attr('id').replace("Comment_", ""))
            if id > (last_post_id or 0):
                Post.objects.create(
                    post_id=id,
                    date=dateparser.parse(comment.select('*//div[contains(@class, "CommentInfo ")]/span').attr('title')),
                    username=comment.select('*//div[@class="AuthorWrap"]//a[contains(@class, "userInfo")]').attr('id'),
                    full_body=comment.html(),
                    body=comment.select('*//div[@class="Message"]').html(),
                )
        try:
            url = response.select('//*[@id="PagerBefore"]/a[contains(@class, "Next")]').attr('href')
        except DataNotFound:
            return
