from django.shortcuts import render

# Create your views here.
from django.views.generic.list import ListView
from .models import Post


class UserPostsView(ListView):
    model = Post

    def get_queryset(self):
        return super().get_queryset().filter(username=self.request.GET.get("author", "").strip())
